package org.drools6.book;

import org.drools.devguide.eshop.model.Item;
import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.internal.utils.KieService;

/**
 * Created by anshulgarg on 06/08/17.
 */
public class App {

    public static void main(String arr[]){
        System.out.println("Bootstrapping the Rule Engine .....");
        KieServices kieServices = KieServices.Factory.get();
        KieContainer kieContainer = kieServices.getKieClasspathContainer();
        KieSession kieSession = kieContainer.newKieSession();
        Item item = new Item("A",123.0,234.0);
        System.out.println("Item Category : "+item.getCategory());

        // Provide Information to Rule Engine Context.
        kieSession.insert(item);
        // Execute the rules that are matching
        int fired = kieSession.fireAllRules();
        System.out.println("Number of Rules Fired : "+fired);
        System.out.println("Item Category : "+item.getCategory());
    }
}
