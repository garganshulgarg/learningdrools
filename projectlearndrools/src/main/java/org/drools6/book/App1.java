package org.drools6.book;

import org.drools.devguide.eshop.model.Item;
import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;
import org.kie.api.cdi.KSession;
import org.kie.api.runtime.KieSession;

import javax.inject.Inject;
import java.io.PrintStream;

/**
 * Created by anshulgarg on 06/08/17.
 */
public class App1 {

    @Inject
    @KSession
    KieSession kieSession;

    public void go(PrintStream out)
    {
        Item item = new Item("A",123.0,234.0);
        out.println("Item Category : "+item.getCategory());
        kieSession.insert(item);
        int fired = kieSession.fireAllRules();
        out.println("Number of Rules Executed : "+fired);
        out.println("Item Category : "+item.getCategory());
    }
    public static void main(String arr[])
    {
        // Bootstrapping the WEDL CDI Container
        Weld weld = new Weld();
        WeldContainer weldContainer = weld.initialize();
        App1 app1 = weldContainer.instance().select(App1.class).get();
        app1.go(System.out);
        weld.shutdown();
    }
}
