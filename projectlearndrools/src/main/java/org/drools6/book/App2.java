package org.drools6.book;

import org.drools.devguide.eshop.model.Customer;
import org.drools.devguide.eshop.model.Item;
import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;
import org.kie.api.cdi.KSession;
import org.kie.api.runtime.KieSession;

import javax.inject.Inject;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import org.drools.devguide.eshop.model.Order;
import org.drools.devguide.eshop.model.OrderLine;

/**
 * Created by anshulgarg on 13/08/17.
 */
public class App2 {

    @Inject
    @KSession
    KieSession kieSession;

    public void go(PrintStream out) {
        Customer customer = createCustomerPOJO();
        Order order = createOrderPOJO(6);
        order.setCustomer(customer);
        System.out.println("Customer Category : " + customer.getCategory());


        kieSession.insert(order);
        // If we didn't insert the customer in KieSession then following condition in classify-customer-rules_1.drl appears to be false.
        // $c: Customer(this == $customer, category==Customer.Category.NA)
        kieSession.insert(customer);

        // If we insert the below mentioned lines item-category-rules_1.drl will also get executed. So, multiple rules got executed.
        for (OrderLine orderLine:order.getOrderLines()) {
            kieSession.insert(orderLine.getItem());
        }

        int fired = kieSession.fireAllRules();
        System.out.println("Number of Rules Executed : " + fired);
        System.out.println("Customer Category After Rules Execution : " + customer.getCategory());
        printOrderDetailsAfterEvaluation(order);
        printCouponDetails(order);


    }




    public static void main(String arr[]) {
        // Bootstrapping the WELD CDI Container
        Weld weld = new Weld();
        WeldContainer weldContainer = weld.initialize();
        App2 app2 = weldContainer.instance().select(App2.class).get();
        app2.go(System.out);
        weld.shutdown();

    }


    public void printOrderDetailsAfterEvaluation(final Order order)
    {
        System.out.println("####  Order Item Details  ####");
        if(null != order.getDiscount()) {
            System.out.println(" Discount If Applicable : " + order.getDiscount().getPercentage());
        }
        for (OrderLine orderLine: order.getOrderLines()) {
            Item item = orderLine.getItem();
            System.out.println(item.getName()+"   "+item.getCost() + "   "+item.getCategory());
        }


    }

    public void printCouponDetails(final Order order)
    {
        System.out.println("Customer Coupon Details");

    }
    public Customer createCustomerPOJO() {
        Customer customer = new Customer();
        customer.setCategory(Customer.Category.NA);
        customer.setName("Customer - 1");
        return customer;
    }

    public Order createOrderPOJO(int numberOfItems) {
        Order order = new Order();
        //order.setCustomer(createCustomerPOJO());
        order.setOrderId(Long.parseLong("1234"));
        order.setItems(createOrderLinePOJO(numberOfItems));
        return order;
    }


    public List<OrderLine> createOrderLinePOJO(int numberOfItems) {
        List<OrderLine> orderLines = new ArrayList<OrderLine>();
        for (int i = 0; i < numberOfItems; i++) {
            OrderLine orderLine = new OrderLine();
            orderLine.setItem(createItemPOJO("Item-" + (i + 1), 100 * (i + 1), 101 * (i + 1)));
            orderLines.add(orderLine);
        }
        return orderLines;
    }


    public Item createItemPOJO(String itemName, double itemCost, double itemSale) {
        Item item = new Item(itemName, itemCost, itemSale);
        return item;
    }

}
